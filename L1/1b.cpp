int main()
{

    int result = 1;
    int a=2;
    int n=4;

    while(n>0) {
        if (n % 2 == 1)result *= a;
        a *= a;
        n /= 2;
    }

    std::cout << result << std::endl;

};