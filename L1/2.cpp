double bi(double a, double b)
{
    double s = a;
    while (b - a >= std::numeric_limits<double>::min())
    {
        s = (b + a) / 2;
        if (wartosc(s) == 0)
        {
            return s;
        }
        else if (wartosc(s) * wartosc(a) < 0)
        {
            b = s;
        }
        else
        {
            a = s;
        }
    };
    return s;
}