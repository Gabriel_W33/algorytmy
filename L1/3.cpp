
double oblicz(double a[], int n, double x)
{
	double result = 0;

	for(int i = n; i > 0; --i)
	{
        result = (a[i] + result) * x;
	}

	return result + a[0];
}
