class ulamek {

public:
    int licz;
    int mian;

    ulamek () {};

    ulamek(int _licz, int _mian = 1)
        : licz(_licz), mian(_mian) {};

    int NWD( int a,  int b) {
        if ((a == 0) or (b == 0)) {
            return 1;
        }
        while(a != b) {
            if (a < b) b -= a; else a -= b;
        }
        return a;
    }


    ulamek operator+ (ulamek u) {
        ulamek t;
        int wsp = mian * u.getmian();
        int templiczmian = (licz * u.getmian()) + (u.getlicz() * mian);
        int dz = NWD(templiczmian, wsp);
        t.setlicz(licz / dz);
        t.setmian(wsp / dz);

        return t;
    }

    ulamek operator- (ulamek u) {
        ulamek t;
        int wsp = mian * u.getmian();
        int templiczmian = (licz * u.getmian()) - (u.getlicz() * mian);
        int dz = NWD(templiczmian, wsp);
        t.setlicz(licz / dz);
        t.setmian(wsp / dz);

        return t;
    }

    ulamek operator* (ulamek u) {
        ulamek t;
        int templicz = licz * u.getlicz();
        int tempmian = mian * u.getmian();
        int dz = NWD(templicz, tempmian);
        t.setlicz(templicz / dz);
        t.setmian(tempmian / dz);

        return t;

    }

    ulamek operator/ (ulamek u) {
        ulamek t;
        int templicz = licz * u.getmian();
        int tempmian = mian * u.getlicz();
        int dz = NWD(templicz, tempmian);
        t.setlicz(templicz / dz);
        t.setmian(tempmian / dz);

        return t;
    }

    int getlicz() {
        return licz;
    }

    int getmian() {
        return mian;
    }

    void setlicz(int _licz) {
        licz = _licz;
    }

    void setmian(int _mian) {
        mian = _mian;
    }


};
