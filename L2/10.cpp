
void insert(Lnode *& L, int x) 
{
    Lnode **pL=&L;
    while(*pL)
        if(x < (*pL)->x)
            pL=&((*pL)->left);
        else
            pL=&((*pL)->right);
    *pL=new Lnode(x);
}

Lnode* find(Lnode* L,int x)  
{
    while((L) and ( L->x!=x))
    {
        if(x < L->x)
            L=L->left;
        else
            L=L->right;
    }
    return L;
}