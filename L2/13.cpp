void inorder_do(node *t, void f(node * x))
{
	if(t){
	        inorder_do(t->left, f);
	        f(t);  
	        inorder_do(t->right, f);
	}
}