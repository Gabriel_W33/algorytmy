int height(node* t)  
{
    if(!t) return 0;
	 
    int counterLeft = height(t->left);
    int counterRight = height(t->right);
   
	 if(counterRight > counterLeft)
        return 1 + counterRight;
    else
        return 1 + counterLeft;
}