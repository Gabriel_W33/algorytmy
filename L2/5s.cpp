void reverse(lnode*& L) 
    { 
      
        lnode* current = L;
        Lnode *prev = NULL; 
  	 		Lnode *next = NULL;
  
        while (current != NULL) 
        { 
            
            next = current->next; 
            
            current->next = prev; 
        
            prev = current; 
				
            current = next; 
        } 
        L = prev; 
    } 