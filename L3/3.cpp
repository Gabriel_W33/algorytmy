#include <iostream>

struct lnode
{
    int key;
    lnode *next;
    lnode(int n=0, lnode *l = NULL) : key(n), next(l) {};
};



void insertion_sort(lnode *&L)
{

    if (L && L->next)
    {
        lnode* sortedL = NULL;
        lnode* unsortedL = L;
        while (unsortedL)
        {
            lnode* value = unsortedL;
            unsortedL = unsortedL->next;
            if (!sortedL || value->key <= sortedL->key)
            {
                value->next = sortedL;
                sortedL = value;
            }
            else
            {
                lnode* curSorted = sortedL;

                while (curSorted)
                {
                    if (!curSorted->next || value->key < curSorted->next->key)
                    {
                        value->next = curSorted->next;
                        curSorted->next = value;
                        break;
                    }
                    curSorted = curSorted->next;
                }
            }
        }
        L = sortedL;
    }
}




void print(lnode *L)
{
    while (L != NULL)
    {
        std::cout << L->key << " ,";
        L = L->next;
    }
    std::cout << std::endl;
}





int main()
{
    lnode *l = NULL;

    l = new lnode(5, l);
    l = new lnode(2, l);
    l = new lnode(4, l);
    l = new lnode(1, l);
	 
    insertion_sort(l);
    print(l);

    return 0;
}